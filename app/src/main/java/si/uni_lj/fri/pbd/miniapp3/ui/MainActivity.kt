package si.uni_lj.fri.pbd.miniapp3.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.adapter.SectionsPagerAdapter

class MainActivity : AppCompatActivity() {

    companion object {
        private const val NUM_OF_TABS =2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        configureTabLayout()
    }

    private fun configureTabLayout() {
        val tabLayout = findViewById<TabLayout>(R.id.tab_layout)
        val viewPager = findViewById<ViewPager2>(R.id.view_pager)
        val tpAdapter = SectionsPagerAdapter(this, NUM_OF_TABS)
        viewPager.adapter = tpAdapter
        TabLayoutMediator(tabLayout, viewPager) {tab, position ->
            when (position) {
                0 -> tab.setText(R.string.tab1_title)
                1 -> tab.setText(R.string.tab2_title)
            }
        }.attach()
    }
}