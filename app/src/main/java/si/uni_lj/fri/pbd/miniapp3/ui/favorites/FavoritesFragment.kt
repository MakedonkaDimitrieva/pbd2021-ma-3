package si.uni_lj.fri.pbd.miniapp3.ui.favorites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails
import si.uni_lj.fri.pbd.miniapp3.models.Mapper
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM
import si.uni_lj.fri.pbd.miniapp3.ui.RecipeViewModel

class FavoritesFragment : Fragment() {

    private lateinit var mViewModel: RecipeViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(RecipeViewModel::class.java)

        // observing changes in favorite recipes
        mViewModel.getFavorites().observe(viewLifecycleOwner, { t -> setRecycler(t!!) })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favorites, container, false)
    }

    private fun setRecycler(recipesDetails: List<RecipeDetails>) {
        val recipeSummaryIMs: MutableList<RecipeSummaryIM> = mutableListOf()
        for(recipeDetails: RecipeDetails in recipesDetails)
            recipeSummaryIMs.add(Mapper.mapRecipeDetailsToRecipeSummaryIm(recipeDetails))
        recipeSummaryIMs.sortWith { o1, o2 -> o1!!.strDrink.compareTo(o2!!.strDrink) }
        val recyclerView = view?.findViewById(R.id.favorites_recycler) as RecyclerView
        recyclerView.setHasFixedSize(true)
        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(context, 1)
        recyclerView.layoutManager=layoutManager
        val recyclerAdapter = RecyclerViewAdapter(requireContext(), recipeSummaryIMs, false)
        recyclerView.adapter=recyclerAdapter
    }
}