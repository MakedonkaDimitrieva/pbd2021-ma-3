package si.uni_lj.fri.pbd.miniapp3.ui.search

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import me.zhanghai.android.materialprogressbar.MaterialProgressBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.adapter.RecyclerViewAdapter
import si.uni_lj.fri.pbd.miniapp3.adapter.SpinnerAdapter
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientsDTO
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator
import si.uni_lj.fri.pbd.miniapp3.ui.RecipeViewModel

class SearchFragment : Fragment() {

    private lateinit var mViewModel: RecipeViewModel
    private lateinit var materialProgressBar: MaterialProgressBar
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var spinner: Spinner
    private var recyclerView: RecyclerView? = null
    private lateinit var mRestAPI: RestAPI
    private var startTime: Long = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_search, container, false)
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        mViewModel = ViewModelProvider(this).get(RecipeViewModel::class.java)
        materialProgressBar = view.findViewById(R.id.material_progress_bar)
        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout)
        spinner = view.findViewById(R.id.spinner)
        recyclerView = view.findViewById(R.id.recycler_view)
        mRestAPI = ServiceGenerator.createService(RestAPI::class.java)
        getIngredients()
        return view
    }

    // fetch ingredients
    private fun getIngredients() {

        mRestAPI.allIngredients!!.enqueue(object : Callback<IngredientsDTO?> {

            override fun onResponse(call: Call<IngredientsDTO?>, response: Response<IngredientsDTO?>) {
                if (response.isSuccessful) {
                    // on success the spinner is set
                    setSpinner(response.body()!!.ingredients!!)
                }
            }

            override fun onFailure(call: Call<IngredientsDTO?>, t: Throwable) {
                // on failure a toast is shown and user can try again with swipe
                Toast.makeText(context, R.string.error_no_internet, Toast.LENGTH_LONG).show()
                swipeRefreshLayout.setOnRefreshListener {
                    swipeRefreshLayout.isRefreshing = false
                    getIngredients()
                }
            }
        })
    }

    // spinner setup
    private fun setSpinner(ingredientsDTOS: List<IngredientDTO>) {

        materialProgressBar.visibility=MaterialProgressBar.INVISIBLE
        val spinnerAdapter = SpinnerAdapter(this, ingredientsDTOS)
        spinner.adapter=spinnerAdapter

        // on ingredient selection, recipes are shown
        spinner.onItemSelectedListener = object : OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                materialProgressBar.visibility=MaterialProgressBar.VISIBLE

                // set recipes in adapter
                setRecipeSummaries(spinnerAdapter, position)
                startTime = System.currentTimeMillis()
                swipeRefreshLayout.setOnRefreshListener {

                    // the layout can be refreshed after 5seconds
                    if(System.currentTimeMillis() - startTime > 5000) {
                        setRecipeSummaries(spinnerAdapter, position)
                        swipeRefreshLayout.isRefreshing=false
                        startTime = System.currentTimeMillis()
                    } else {
                        swipeRefreshLayout.isRefreshing=false
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    private fun setRecipeSummaries(spinnerAdapter: SpinnerAdapter, position: Int) {
        mViewModel.getRecipeSummaries(true, (spinnerAdapter.getItem(position) as IngredientDTO).strIngredient1!!)
                .observe(viewLifecycleOwner, { t ->
                    materialProgressBar.visibility = MaterialProgressBar.INVISIBLE
                    if (!isNetworkAvailable()) Toast.makeText(context, R.string.error_no_internet, Toast.LENGTH_LONG).show()
                    else if (t == null) {
                        Toast.makeText(context, R.string.error_no_recipes_for_ingredient, Toast.LENGTH_LONG).show()
                    }
                    setRecycler(t)
                })
    }

    private fun setRecycler(recipeSummaryIMS: List<RecipeSummaryIM>?) {
        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(context, 1)
        recyclerView?.layoutManager = layoutManager
        val recyclerAdapter = RecyclerViewAdapter(requireContext(), recipeSummaryIMS!!, true)
        recyclerView?.adapter = recyclerAdapter
        recyclerView?.setHasFixedSize(true)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = requireActivity().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetwork
        val activeNetworkInfo = connectivityManager.getNetworkCapabilities(networkInfo)
        return when {
            activeNetworkInfo!!.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            activeNetworkInfo.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            else -> false
        }
    }
}