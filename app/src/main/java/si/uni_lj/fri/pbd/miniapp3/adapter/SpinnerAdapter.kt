package si.uni_lj.fri.pbd.miniapp3.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.models.dto.IngredientDTO
import si.uni_lj.fri.pbd.miniapp3.ui.search.SearchFragment

class SpinnerAdapter(private var context: SearchFragment, private var ingredients: List<IngredientDTO>) : BaseAdapter() {

    override fun getCount(): Int {
        return ingredients.size
    }

    override fun getItem(position: Int): Any {
        return ingredients[position]
    }

    override fun getItemId(position: Int): Long {
        return position+0L
    }

    // create Holder for ingredient and add it to the view
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = Holder()
        var mView: View? = convertView
        if(mView == null) {
            val inflater = context.activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            mView = inflater.inflate(R.layout.spinner_item, parent, false)
            holder.ingredient = mView.findViewById(R.id.text_view_spinner_item)
            mView.tag=holder
        } else {
            holder = mView.tag as Holder
        }
        val ingredientDTO = getItem(position) as IngredientDTO
        holder.ingredient?.text=ingredientDTO.strIngredient1
        return mView!!
    }

    // class to hold the view
    inner class Holder {
        var ingredient: TextView? = null
    }

}