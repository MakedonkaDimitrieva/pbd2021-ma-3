package si.uni_lj.fri.pbd.miniapp3.ui

import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import android.widget.ToggleButton
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import butterknife.ButterKnife
import com.bumptech.glide.Glide
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM

class DetailsActivity : AppCompatActivity() {

    private lateinit var mViewModel: RecipeViewModel
    private lateinit var detailsImage: ImageView
    private lateinit var detailsTitle: TextView
    private lateinit var detailsToggleButton: ToggleButton
    private lateinit var detailsIngredients: TextView
    private lateinit var detailsInstructions: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        ButterKnife.bind(this)
        mViewModel = ViewModelProvider(this).get(RecipeViewModel::class.java)
        detailsImage = findViewById(R.id.details_image)
        detailsTitle = findViewById(R.id.details_title)
        detailsToggleButton = findViewById(R.id.details_favorite_button)
        detailsIngredients = findViewById(R.id.ingredients_list)
        detailsInstructions = findViewById(R.id.details_instructions)

        val drinkId : String = intent.getStringExtra("drinkId").toString()
        val fromAPI: Boolean = intent.getBooleanExtra("fromAPI", true)

        // fetching data
        mViewModel.getRecipeDetailsById(drinkId, fromAPI).observe(this, { t -> setDetails(t) })
    }

    private fun setDetails(recipeDetails: RecipeDetailsIM?) {
        Glide.with(this).load(recipeDetails?.strDrinkThumb).into(this.detailsImage)
        detailsTitle.text=recipeDetails?.strDrink
        recipeDetails?.getFavorite()?.let { setFavorite(it) }
        detailsIngredients.text= recipeDetails?.let { ingredientsStringBuild(it) }
        detailsInstructions.text= recipeDetails?.strInstructions

        // ui setup
        detailsToggleButton.setOnClickListener {
            if (recipeDetails?.getFavorite()!!) {
                detailsToggleButton.isChecked = false
                recipeDetails.setFavorite(false)
                mViewModel.deleteRecipe(recipeDetails.idDrink!!)
            } else {
                detailsToggleButton.isChecked = true
                recipeDetails.setFavorite(true)
                mViewModel.insertRecipe(recipeDetails)
            }
        }
    }

    // setup of the toggle button layout
    private fun setFavorite(isFavorite: Boolean) {
        detailsToggleButton.isChecked = isFavorite
    }

    // showing the ingredients and their measurements together
    private fun ingredientsStringBuild(rd: RecipeDetailsIM): String {
        val builder = StringBuilder()
        if (rd.strIngredient1 != null && rd.strIngredient1 != "") {
            builder.append("""  • ${rd.strIngredient1}${measuresStringBuild(rd.strMeasure1!!)}""".trimIndent())
        }
        if (rd.strIngredient2 != null && rd.strIngredient2 != "") {
            builder.append("""  • ${rd.strIngredient2}${measuresStringBuild(rd.strMeasure2!!)}""".trimIndent())
        }
        if (rd.strIngredient3 != null && rd.strIngredient3 != "") {
            builder.append("""  • ${rd.strIngredient3}${measuresStringBuild(rd.strMeasure3!!)}""".trimIndent())
        }
        if (rd.strIngredient4 != null && rd.strIngredient4 != "") {
            builder.append("""  • ${rd.strIngredient4}${measuresStringBuild(rd.strMeasure4!!)}""".trimIndent())
        }
        if (rd.strIngredient5 != null && rd.strIngredient5 != "") {
            builder.append("""  • ${rd.strIngredient5}${measuresStringBuild(rd.strMeasure5!!)}""".trimIndent())
        }
        if (rd.strIngredient6 != null && rd.strIngredient6 != "") {
            builder.append("""  • ${rd.strIngredient6}${measuresStringBuild(rd.strMeasure6!!)}""".trimIndent())
        }
        if (rd.strIngredient7 != null && rd.strIngredient7 != "") {
            builder.append("""  • ${rd.strIngredient7}${measuresStringBuild(rd.strMeasure7!!)}""".trimIndent())
        }
        if (rd.strIngredient8 != null && rd.strIngredient8 != "") {
            builder.append("""  • ${rd.strIngredient8}${measuresStringBuild(rd.strMeasure8!!)}""".trimIndent())
        }
        if (rd.strIngredient9 != null && rd.strIngredient9 != "") {
            builder.append("""  • ${rd.strIngredient9}${measuresStringBuild(rd.strMeasure9!!)}""".trimIndent())
        }
        if (rd.strIngredient10 != null && rd.strIngredient10 != "") {
            builder.append("""  • ${rd.strIngredient10}${measuresStringBuild(rd.strMeasure10!!)}""".trimIndent())
        }
        if (rd.strIngredient11 != null && rd.strIngredient11 != "") {
            builder.append("""  • ${rd.strIngredient11}${measuresStringBuild(rd.strMeasure11!!)}""".trimIndent())
        }
        if (rd.strIngredient12 != null && rd.strIngredient12 != "") {
            builder.append("""  • ${rd.strIngredient12}${measuresStringBuild(rd.strMeasure12!!)}""".trimIndent())
        }
        if (rd.strIngredient13 != null && rd.strIngredient13 != "") {
            builder.append("""  • ${rd.strIngredient13}${measuresStringBuild(rd.strMeasure13!!)}""".trimIndent())
        }
        if (rd.strIngredient14 != null && rd.strIngredient14 != "") {
            builder.append("""  • ${rd.strIngredient14}${measuresStringBuild(rd.strMeasure14!!)}""".trimIndent())
        }
        if (rd.strIngredient15 != null && rd.strIngredient15 != "") {
            builder.append("""  • ${rd.strIngredient15}${measuresStringBuild(rd.strMeasure15!!)}""".trimIndent())
        }
        return builder.toString()
    }

    // builder for measurements, some recipes do not have measurements
    private fun measuresStringBuild(m: String): String {
        var measure: String? = m
        if (measure != null) {
            while (measure!!.endsWith(" ")) {
                measure = measure.substring(0, measure.length - 1)
            }
            if (measure != "") {
                return "  ($measure)"
            }
        }
        return ""
    }
}
