package si.uni_lj.fri.pbd.miniapp3.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import si.uni_lj.fri.pbd.miniapp3.database.Repository
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM

class RecipeViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = Repository(application)

    fun getRecipeDetailsById(id: String, fromAPI: Boolean): MutableLiveData<RecipeDetailsIM> {
        return repository.getRecipeDetails(id, fromAPI)
    }

    fun getRecipeSummaries(fromAPI: Boolean, ingredient: String): MutableLiveData<List<RecipeSummaryIM>> {
        return repository.getRecipeSummaries(fromAPI, ingredient)
    }

    fun deleteRecipe(idDrink: String) {
        repository.deleteRecipe(idDrink)
    }

    fun insertRecipe(recipeDetailsIM: RecipeDetailsIM) {
        repository.insertRecipe(recipeDetailsIM)
    }

    fun getFavorites(): LiveData<List<RecipeDetails>?> {
        return repository.getFavorites()
    }
}