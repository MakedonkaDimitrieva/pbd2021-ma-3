package si.uni_lj.fri.pbd.miniapp3.adapter

import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import si.uni_lj.fri.pbd.miniapp3.R
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM
import si.uni_lj.fri.pbd.miniapp3.ui.DetailsActivity

class RecyclerViewAdapter(private var context: Context, private var recipeSummaries: List<RecipeSummaryIM>, private var fromAPI: Boolean) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    // inflate the the layout grid items with ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.layout_grid_item, parent, false)
        return ViewHolder(view)
    }

    // bind recipes to ViewHolder
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recipeSummary: RecipeSummaryIM = recipeSummaries[position]
        holder.setImage(recipeSummary.strDrinkThumb, context)
        holder.setTitle(recipeSummary.strDrink)

        // on chosen recipe, show the details
        holder.itemView.setOnClickListener {
            if (!fromAPI||isNetworkAvailable()) {
                val intent = Intent(context, DetailsActivity::class.java)
                intent.putExtra("drinkId", recipeSummary.idDrink)
                intent.putExtra("fromAPI", fromAPI)
                context.startActivity(intent)
            } else {
                Toast.makeText(context, R.string.error_no_internet, Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun getItemCount(): Int {
        return recipeSummaries.size
    }

    // class that holds the views
    inner class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        private var itemImage: ImageView? = null
        private var itemTitle: TextView? = null

        init {
            itemImage = itemView?.findViewById(R.id.image_view)
            itemTitle = itemView?.findViewById(R.id.text_view_content)
        }

        fun setImage(strDrinkThumb: String, context: Context) {
            Glide.with(context).load(strDrinkThumb).into(this.itemImage!!)
        }

        fun setTitle(strDrink: String) {
            this.itemTitle!!.text = strDrink
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetwork
        val activeNetworkInfo = connectivityManager.getNetworkCapabilities(networkInfo)
        return when {
            activeNetworkInfo!!.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            activeNetworkInfo.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            else -> false
        }
    }
}