package si.uni_lj.fri.pbd.miniapp3.database

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import si.uni_lj.fri.pbd.miniapp3.database.dao.RecipeDao
import si.uni_lj.fri.pbd.miniapp3.database.entity.RecipeDetails
import si.uni_lj.fri.pbd.miniapp3.models.Mapper
import si.uni_lj.fri.pbd.miniapp3.models.Mapper.mapRecipeDetailsDtoToRecipeDetails
import si.uni_lj.fri.pbd.miniapp3.models.Mapper.mapRecipeDetailsToRecipeSummaryIm
import si.uni_lj.fri.pbd.miniapp3.models.RecipeDetailsIM
import si.uni_lj.fri.pbd.miniapp3.models.RecipeSummaryIM
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipeDetailsDTO
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIdDTO
import si.uni_lj.fri.pbd.miniapp3.models.dto.RecipesByIngredientDTO
import si.uni_lj.fri.pbd.miniapp3.rest.RestAPI
import si.uni_lj.fri.pbd.miniapp3.rest.ServiceGenerator
import java.util.*

class Repository(application: Application?) {

    private val mRestClient: RestAPI = ServiceGenerator.createService(RestAPI::class.java)
    private val recipeDao: RecipeDao
    private val favorites: LiveData<List<RecipeDetails>?>
    val recipeDetails: RecipeDetails? = null

    init {
        val db: Database = Database.getDatabase(application?.applicationContext!!)!!
        recipeDao = db.recipeDao()
        favorites = recipeDao.getFavoriteRecipes()
    }

    // fetching recipe summaries
    fun getRecipeSummaries(fromAPI: Boolean, ingredient: String):MutableLiveData<List<RecipeSummaryIM>> {
        val recipeSummaries: MutableLiveData<List<RecipeSummaryIM>> = MutableLiveData<List<RecipeSummaryIM>>()
        if(fromAPI) {
            // fetching from rest client
            mRestClient.getRecipesByIngredient(ingredient)?.enqueue(object : Callback<RecipesByIngredientDTO?> {

                override fun onResponse(call: Call<RecipesByIngredientDTO?>, response: Response<RecipesByIngredientDTO?>) {
                    if (response.isSuccessful) {
                        val temp: MutableList<RecipeSummaryIM> = mutableListOf()
                        if (response.body()?.recipes == null) {
                            recipeSummaries.value = null
                        } else {
                            for (rdDTO: RecipeDetailsDTO in response.body()?.recipes!!) {
                                temp.add(mapRecipeDetailsToRecipeSummaryIm(mapRecipeDetailsDtoToRecipeDetails(rdDTO)))
                            }
                        }
                        recipeSummaries.value = temp
                    } else {
                        recipeSummaries.value = null
                    }
                }

                override fun onFailure(call: Call<RecipesByIngredientDTO?>, t: Throwable) {
                    recipeSummaries.value = null
                }
            })
        }
        // fetching from database
        else {
            val temp: MutableList<RecipeSummaryIM> = mutableListOf()
            for(rd: RecipeDetails in Objects.requireNonNull(favorites.value)!!) {
                temp.add(mapRecipeDetailsToRecipeSummaryIm(rd))
            }
            recipeSummaries.value=temp
        }
        return recipeSummaries
    }

    // fetching recipe details
    fun getRecipeDetails(recipeId: String, fromAPI: Boolean):MutableLiveData<RecipeDetailsIM> {
        val returnRecipe: MutableLiveData<RecipeDetailsIM> = MutableLiveData<RecipeDetailsIM>()
        if(!fromAPI) {
            // fetching from db
            returnRecipe.value= getRecipeById(recipeId)?.let { Mapper.mapRecipeDetailsToRecipeDetailsIm(it) }
        } else {
            // fetching from rest client
            val rd: RecipeDetails? = getRecipeById(recipeId)

            mRestClient.getRecipesById(recipeId)?.enqueue(object : Callback<RecipesByIdDTO?> {
                override fun onResponse(call: Call<RecipesByIdDTO?>, response: Response<RecipesByIdDTO?>) {
                    if (response.isSuccessful) {
                        val recipe: RecipeDetailsIM? = response.body()?.recipesById?.get(0)?.let { Mapper.mapRecipeDetailsDtoToRecipeDetailsIm(true, it) }

                        when (checkForFavorites(recipe, rd)) {
                            "IN DB OK" -> returnRecipe.value = rd?.let { Mapper.mapRecipeDetailsToRecipeDetailsIm(it) }
                            "IN DB NOT OK" -> {
                                deleteRecipe(recipeId)
                                insertRecipe(recipe!!)
                                returnRecipe.value = rd?.let { Mapper.mapRecipeDetailsToRecipeDetailsIm(it) }
                            }
                            else -> returnRecipe.value = response.body()?.recipesById?.get(0)?.let { Mapper.mapRecipeDetailsDtoToRecipeDetailsIm(false, it) }
                        }
                    } else {
                        returnRecipe.value = null
                    }
                }

                override fun onFailure(call: Call<RecipesByIdDTO?>, t: Throwable) {
                    returnRecipe.value = null
                }
            })
        }
        return returnRecipe
    }

    // check whether the recipe from api call is the same the one from db
    private fun checkForFavorites(recipe: RecipeDetailsIM?, rd: RecipeDetails?):String{
        if(rd != null) {
            return if(recipe == Mapper.mapRecipeDetailsToRecipeDetailsIm(rd)) {
                "IN DB OK"
            } else {
                "IN DB NOT OK"
            }
        }
        return "NOT IN DB"
    }

    private fun getRecipeById(recipeId: String) : RecipeDetails? {
        Database.databaseWriteExecutor.execute {
            recipeDetails.apply { recipeDao.getRecipeById(recipeId) }
        }
        return recipeDetails
    }

    fun insertRecipe(recipeDetailsIM: RecipeDetailsIM) {
        val recipeDetails: RecipeDetails = Mapper.mapRecipeDetailsIMToRecipeDetails(recipeDetailsIM.getFavorite(), recipeDetailsIM)
        Database.databaseWriteExecutor.execute {
            recipeDao.insertRecipe(recipeDetails)
        }
    }

    fun deleteRecipe(idDrink: String) {
        Database.databaseWriteExecutor.execute {
            recipeDao.deleteRecipe(idDrink)
        }
    }

    fun getFavorites(): LiveData<List<RecipeDetails>?> {
        return favorites
    }
}